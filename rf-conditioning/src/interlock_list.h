#ifndef INTERLOCKLIST_H
#define INTERLOCKLIST_H

#include <aSubRecord.h>
#include <alarm.h>
#include <dbAccess.h>
#include <dbChannel.h>
#include <devSup.h>
#include <epicsExport.h>
#include <errlog.h>
#include <menuFtype.h>
#include <registryFunction.h>

#include <algorithm>
#include <cmath>
#include <cstddef>
#include <cstring>
#include <ctime>
#include <iostream>
#include <memory>
#include <vector>

// PVXS headers
#include <pvxs/iochooks.h>
#include <pvxs/log.h>
#include <pvxs/nt.h>
#include <pvxs/server.h>
#include <pvxs/client.h>
#include <pvxs/sharedpv.h>

/*
 * Structure used to print FIM interlock messages from SNL
 * machine program in the OPI
 **/
struct InterlockList {
  size_t maxSize;
  std::deque<std::string> elements;

  const std::string pvName;
  pvxs::server::SharedPV outputPV;

  pvxs::client::Context ctxt;

  InterlockList(const char *pvname, int initialSize);
  std::string createMessage(const std::string &chanName);
  void addElement(const std::string &newElement);
  void setSize(int newSize);
  void updateNTPV();
};

// EPICS aSub routines
long interlock_list_init(aSubRecord *prec);
long interlock_list_proc(aSubRecord *prec);

#endif // InterlockList_H
