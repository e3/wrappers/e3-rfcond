/* DTL Conditioning SNL Program-- */

/* Author: Emmanouil Trachanas*
/* Email:  Emmanouil.Trachanas@ess.eu */

/*Previous Versions: Emmanouil Trachanas (RFQ-ESS, DTL1-ESS), Muyuan Wang (TS2-ESS), A. Gaget (CEA) */

program sncConditioning

/* C language libraries */
%% #include <string.h>
%% #include <time.h>
%% #include <stdlib.h>
%% #include <stdio.h>
%% #include "sncConditioning.h"
%% #include <unistd.h>



/*=================== Options ======================*/
option + r;
option + s;
option - c;
/*=================================================*/

/*=================== Declarations ======================*/

/* start/stop Button */
int startButton;
assign startButton to "{M}:Button";
monitor startButton;

/* Reset  Button */
int resetButton;
assign resetButton to "{M}:ResetButton";
monitor resetButton;

/*Infinite Procedure Button */
int neverStop;
assign neverStop to "{M}:Inf";
monitor neverStop;

/* Status statusMessage */
string statusMessage;
assign statusMessage to "{M}:Msg";
monitor statusMessage;

/* power control parameters */

/* Minimum Power */

float powerMin;
assign powerMin to "{M}:Power-min";
monitor powerMin;

/* Maximum Power */
float powerMax;
assign powerMax to "{M}:Power-max";
monitor powerMax;

/*Power Floor-the power cannot be reduced more */
float powerFloor;
assign powerFloor to "{M}:Power-Floor";
monitor powerFloor;

/*Power Step Up */
float powerStepUp;
assign powerStepUp to "{M}:Power-Step-Up";
monitor powerStepUp;

/*Power Step Down */
float powerStepDown;
assign powerStepDown to "{M}:Power-Step-Down";
monitor powerStepDown;

/*Power LLRF Setpoint */
float powerKlystron;
assign powerKlystron to "{LLRF}FFPulseGenP";
monitor powerKlystron;

/*---------------------------------*/

/*RF pulse Control Parameters*/

/* Plateau Time */
float pulsePlateauTime;
assign pulsePlateauTime to "{M}:Plateau-Time";
monitor pulsePlateauTime;

/* RF Pulse width Step */
float pulseWidthStep;
assign pulseWidthStep to "{M}:Pulse-Width-Step";
monitor pulseWidthStep;

/* Minimum RF Pulse width */
float pulseWidthMin;
assign pulseWidthMin to "{M}:Pulse-Width-Min";
monitor pulseWidthMin;

/* Maximum RF Pulse width */
float pulseWidthMax;
assign pulseWidthMax to "{M}:Pulse-Width-Max";
monitor pulseWidthMax;

/* Count Step Pulse */
float pulseStepTotal;
assign pulseStepTotal to "{M}:Count-Pulse-Step";
monitor pulseStepTotal;

/* width-flag- When enabled if an interlock causes P<powerMin for a certain Wi then also the width decreases and starts from powerMax */
float widthFlag;
assign widthFlag to "{M}:width-flag";
monitor widthFlag;

/*Interlock Detection*/

/* Critical Fault, Brings down the LPS and LLRF state machine */
int faultCritical;
assign faultCritical to "{M}:Critical-Fault";
monitor faultCritical;

/* Critical vacuum fault */

int faultCriticalVacuum;
assign faultCriticalVacuum to "{M}:Critical-Vacuum-Fault";
monitor faultCriticalVacuum;

/* faultLight (Reflected power or cavity decay)---> Leads to the reduction power and/or width */

int faultLight;
assign faultLight to "{M}:Light-Fault";
monitor faultLight;

/* Vacuum Fault -- Comparison with the Soft threshold */

int faultVacuum;
assign faultVacuum to "{M}:Vacuum-Fault";
monitor faultVacuum;

/* Arc Detectors */

int arcfault;
assign arcfault to "{M}:Arc-Fault";
monitor arcfault;

/*Settings to EVM RF system*/

/* Repetition Rate Set Point */

float repetitionRate;
assign repetitionRate to "{EVR}MixFreq-RB";

/* RF Pulse width Set Point */

float pulseWidth;
assign pulseWidth to "{EVR}RFSyncWdt-SP";

/* Count Pulse */

int countPulse;
assign countPulse to "{EVR}Cycle-Cnt";
monitor countPulse;

/* Interlock Count Cavity Decay--(no use) */

int cavityDecayInterlockCount;
assign cavityDecayInterlockCount to "{FIM}CD1-FastRst-IlckCnt";
monitor cavityDecayInterlockCount;

/* Interlock Count Reflected Power--(no use) */
int reflectedPowerInterlockCount;
assign reflectedPowerInterlockCount to "{FIM}RP1-FastRst-IlckCnt";
monitor reflectedPowerInterlockCount;

/* Autoreset Variables */

/*LPS State Machine*/

int fimState;
assign fimState "{RFS}FIM-101:FSM";
monitor fimState;

/* FIM Reset */

int fimReset;
assign fimReset to "{FIM}Rst";
monitor fimReset;

/* SIM Reset */

int simReset;
assign simReset to "{RFS}CPU-RST";
monitor simReset;

int simReset2;
assign simReset2 to "{RFS}SIM-110:Reset";
monitor simReset2;

/* LLRF Reset Digitizers */

int llrfState;
assign llrfState to "{RFS}DIG-101:FSM.RVAL";
monitor llrfState;

string llrfSetStateDig1;
assign llrfSetStateDig1 to "{RFS}DIG-101:SMsg";
monitor llrfSetStateDig1;

string llrfSetStateDig2;
assign llrfSetStateDig2 to "{RFS}DIG-102:SMsg";
monitor llrfSetStateDig2;

string llrfSetStateDig3;
assign llrfSetStateDig3 to "{RFS}DIG-103:SMsg";
monitor llrfSetStateDig3;

/* RF ON */

int setSimRfOn;
assign setSimRfOn to "{RFS}CPU-RFON";
monitor setSimRfOn;

string  digitizer;
assign digitizer to "{RFS}DIG-101:FSM";
monitor digitizer;

int  digitizersp;
assign digitizersp to "{RFS}DIG-101:FSM-SP";
monitor digitizersp;



/* Recovery Times */

/* Time after FIM Interlock */

float fimRecoveryTime;
assign fimRecoveryTime to "{M}:fim-time";
monitor fimRecoveryTime;

/* Recovery  Time after Vacuum Interlock */

float vacuumRecoveryTime;
assign vacuumRecoveryTime "{M}:vac-time";
monitor vacuumRecoveryTime;

/* If enabled, sequence iterates from powerMin to powerMax with pulse legth = pulseWidthMax perpetually once reached for the first time */
float maxWidthConditioning;
assign maxWidthConditioning to "{M}:stay";
monitor maxWidthConditioning;

/* Filament Check- Sequence quits if Filament is disabled */

int filamentStatus;
assign filamentStatus to "{RFS}CPU-STBYMISUCND-RB";
monitor filamentStatus;

/* Black List of PVs that should not be rearmed */

int rearmBlackList;
assign rearmBlackList to "{M}:Rearm_final";
monitor rearmBlackList;

/*=================================================*/

/*=================================================*/

/*==================== Variable Settings ====================*/
int countPulseStep = 0;
int operationStart = 1; // start operation;
int powerFloorReached = 0; // power floor;
int soakPlateauReached = 0; // plateau state;
int cycleCount = 0; // cycle counter(Debug Mode);
int operationStop = 0; // operation stop flag;
int printStartMessageFlag = 0; // Flag used to print start message only once
int resetEvent = 0; // Reset request received - to be handled;
int isFirstCycle = 0; // first run;
int faultVacuumFlag = 0; // vacuum fault flag;
int faultVacuumCriticalFlag = 0; // vacuum critical fault flag;
int faultCriticalFlag = 0; // Critical fault vacuum flag;
int doubleReducedPower = 0; // Doubling the reduced power in next step after light fault
float tempPower = 10; // Temp variable to store power
float tempWidth = 10; // Temp variable to store pulse length
int rearmCount = 0; // Counter of rearms
int enuma = 0; // Special scenario flag
int mulrearm = 0; // Special scenario flag
int delayTime = 0; // delay variable
int interlockFlag = 0; // interlock flag 2
int picted = 1; // Terminal single message depiction flag 1
int floorSetCount = 0; // powerFloorReached value set counter
int interlockCount = 0; // counter
int rearmLimitReached = 0; // flag for rearm counter >10
float tempwid = 0.0; //Temp variable to store pulse length-alternative
int fmr=0 ; //Floor manual reset flag



/*====================== Macros ======================*/

#define FLOOR_SET_THRESHOLD 10

#define floorSetLimitResetState() \
    fimState = HVON;               \
    pvPut(fimState);            \
    sprintf(statusMessage, "Manual Reset Requested...\n"); \
    pvPut(statusMessage); \
    floorSetCount = 0;          \
    pulseWidth = pulseWidthMin; \
    fimState = HVON;               \
    pvPut(fimState);            \
    fmr = 1;  \


#define filamentOffProcedure()                              \
    sprintf(statusMessage, "Filament off, quitting...\n");  \
    pvPut(statusMessage);                                   \
    operationStop = 1;                                      \
    pulseWidth = pulseWidthMin;                             \
    pvPut(pulseWidth, SYNC);                                \
    startButton = 0;                                        \
    powerKlystron = powerMin;                               \
    pvPut(powerKlystron, SYNC); \                            \

/*====================== State Sets ======================*/

ss RFConditioning {

    state init {

        /*====================== Operator Stop handler ======================*/

        when(floorSetCount > FLOOR_SET_THRESHOLD) {
            floorSetLimitResetState();

        }
        state init


        when(operationStop == 1 && startButton == 0) {

            operationStop = 0;
        }
        state init

        /*====================== Reset  handler ======================*/

        when(resetEvent == 1 && resetButton == 1) {

            sprintf(statusMessage, "Reset\n");
            pvPut(statusMessage);
            resetEvent = 0;
            resetButton = 0;
            pvPut(resetButton);

        }
        state CycleStart

        /*====================RF state machine collapse===========================*/

        when(startButton == 0 && ((faultCritical == 1) || (arcfault == 1) || (faultLight == 1) || (faultVacuum == 1))) {

        }
        state init

        when(rearmBlackList == 1) {

        }
        state init



       when(faultCritical == 1 && startButton == 1 && rearmBlackList == 0) {

            faultCriticalFlag = 1;
            tempPower = pvGet(powerKlystron);
            tempWidth = pvGet(pulseWidth);
            delayTime = fimRecoveryTime;

            if (faultCriticalVacuum == 1) {

                delayTime = vacuumRecoveryTime;

            }

        }
        state rearm

        /*==================faultLight and faultVacuum handlers===================*/
        /* Light fault and faultVacuum without loss of the state machine */

        when((faultCritical == 0) && ((faultLight == 1) || (arcfault == 1) || (faultVacuum == 1))) {

            interlockFlag = 1;
         // tempPower = pvGet(powerKlystron);
         // tempWidth = pvGet(pulseWidth);


        }
        state PowerControl



        when(operationStop == 0 && startButton == 0 && printStartMessageFlag == 0) {
            printStartMessageFlag = 1;
            sprintf(statusMessage, "Press Start\n");
            pvPut(statusMessage);
        }
        state init

        /*==================Commencing the sequence ===================*/

        when(startButton == 1 && faultCritical == 0 && arcfault == 0 && faultLight == 0 && faultVacuum == 0) {
            sprintf(statusMessage, "Sequence starts..\n");
            pvPut(statusMessage);
	    fmr=0;
        }
        state CycleStart

    }

    /*============================================================*/

    state CycleStart {

        when(cycleCount == 2) {
            sprintf(statusMessage, "End of Sequence\n");
            pvPut(statusMessage);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);
            startButton = 0;
            pvPut(startButton, SYNC);
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            powerFloorReached = 0;
            soakPlateauReached = 0;
            operationStop = 0;
            faultVacuumCriticalFlag = 0;
            if (faultVacuumFlag == 1) {
                faultVacuumFlag = 0;
            }
            operationStart = 1;
            cycleCount = 0;

        }
        state init

//	when(filamentStatus==0){
//		filamentOffProcedure();
//	}
//	state init

	/*  Quit if filament is not enabled */

        when(rearmBlackList == 1) {

            sprintf(statusMessage, "Manual Reset Requested...\n");
            pvPut(statusMessage);
            operationStop = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            startButton = 0;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(startButton == 0) {

            sprintf(statusMessage, "Operator Stop\n");
            pvPut(statusMessage);
            operationStop = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        /* if powerFloorReached value is set more than 10 times- drop the LPS state machine */

        when(floorSetCount > FLOOR_SET_THRESHOLD) {
            floorSetLimitResetState();

        }
        state CycleStart

        when(resetButton == 1) {
            resetEvent = 1;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(faultCritical == 1 && rearmBlackList == 0) {
            faultCriticalFlag = 1;
            tempPower = pvGet(powerKlystron);
            tempWidth = pvGet(pulseWidth);
            delayTime = fimRecoveryTime;
            if (faultCriticalVacuum == 1) {
                delayTime = vacuumRecoveryTime;
            }

        }
        state rearm

        when((faultCritical == 0) && ((faultLight == 1) || (arcfault == 1) || (faultVacuum == 1))) {
            interlockFlag = 1;
         // tempPower = pvGet(powerKlystron);
         // tempWidth = pvGet(pulseWidth);
            if (faultCriticalVacuum == 1)

                if (faultCriticalVacuum == 1) {
                    faultVacuumCriticalFlag = 1;
                }

        }
        state PowerControl

        when((faultCritical == 0) && (faultVacuum == 0) && (arcfault == 0) && (faultLight == 0)) {
            picted = 1;
            if (operationStop == 0) {
                pvGet(powerMin);
                powerKlystron = powerMin;
                pvPut(powerKlystron, SYNC);
            } else {

                powerKlystron = tempPower;
                pvPut(powerKlystron, SYNC);
                pulseWidth = tempWidth;
                pvPut(pulseWidth, SYNC);
                operationStop = 0;
            }

            faultCriticalFlag = 0;
            isFirstCycle = 1;
            if (faultVacuumCriticalFlag == 1) {
                faultVacuumCriticalFlag = 0;
            }
            if ((operationStart == 1) && (operationStop == 0)) {

                countPulseStep = 0;
                pulseWidth = pulseWidthMin;
                pvPut(pulseWidth, SYNC);

                operationStart = 0;
            }

            if ((operationStart == 0) && (operationStop == 0)) {

                //part of code to recover with mimimum pulse length after 5 unsuccessful rearms

                if (mulrearm == 1 && enuma < 1) {
                    pulseWidth = 10.0;
                    enuma = enuma + 1;

                }

                pvPut(pulseWidth, SYNC);
                powerFloorReached = 0;
                soakPlateauReached = 0;

                if (faultVacuumFlag == 1) {
                    faultVacuumFlag = 0;
                }
            }

        }
        state PowerControl
    }
    /*=======================Power Application for a number of cycles=====================================*/

    state nominal {

        when(startButton == 0) {

            sprintf(statusMessage, "Operator Stop\n");
            pvPut(statusMessage);
            operationStop = 1;
            picted = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

     //   when(filamentStatus == 0) {
    //        filamentOffProcedure();
    //    }
    //    state init



 	when(floorSetCount > FLOOR_SET_THRESHOLD) {
            floorSetLimitResetState();
        }
        state init

        /* Manual Reset request in case of black list interlock */

        when(rearmBlackList == 1) {

            sprintf(statusMessage, "Manual Reset Requested...\n");
            pvPut(statusMessage);
            operationStop = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            startButton = 0;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(resetButton == 1) {
            resetEvent = 1;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(faultCritical == 1 && rearmBlackList == 0) {

            faultCriticalFlag = 1;

            delayTime = fimRecoveryTime;
            if (faultCriticalVacuum == 1) {
                delayTime = vacuumRecoveryTime;
            }

        }
        state rearm

        when(soakPlateauReached == 1 && faultCritical == 0 && arcfault == 0 && faultVacuum == 0 && faultLight == 0) {
	}
        state PowerControl

        //Vacuum Fault recovery
        when(faultVacuum == 0 && faultVacuumFlag == 1) {
            faultVacuumFlag = 0;
        }
        state PowerControl



        /* Apply power for a predefined number of cycles */
        when(countPulse - countPulseStep >= pulseStepTotal) {
            rearmCount = 0;
            if (isFirstCycle == 1) {

                isFirstCycle = 0;
            } else {
                if (powerFloorReached == 0) {
                    if (powerKlystron >= powerFloor) {
                        powerFloorReached = 1;
                    }
                }
            }

        }
        state PowerControl

    }

    /*================Power Adjustment Routine============================================*/
    state PowerControl {

        when(startButton == 0) {

            sprintf(statusMessage, "Operator Stop\n");
            pvPut(statusMessage);
            operationStop = 1;
            picted = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

       // when(filamentStatus == 0) {
      //      filamentOffProcedure();
      //  }
       // state init

        when(rearmBlackList == 1) {

            sprintf(statusMessage, "Manual Reset Requested...\n");
            pvPut(statusMessage);
            operationStop = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            startButton = 0;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(resetButton == 1) {
            resetEvent = 1;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);
            operationStop = 1;
            picted = 1;

        }
        state init

        when(faultCritical == 1 && rearmBlackList == 0) {
            faultCriticalFlag = 1;
            tempPower = pvGet(powerKlystron);
            tempWidth = pvGet(pulseWidth);
            delayTime = fimRecoveryTime;
            if (faultCriticalVacuum == 1) {
                delayTime = vacuumRecoveryTime;
            }

        }
        state rearm

        when(floorSetCount > FLOOR_SET_THRESHOLD) {
            floorSetLimitResetState();
        }
        state PowerControl

        when(powerKlystron >= powerMax && faultCritical == 0 && arcfault == 0 && faultVacuum == 0 && faultLight == 0) {

            if (soakPlateauReached == 1) {
                sprintf(statusMessage, "Reached Plateau\n");
                pvPut(statusMessage);
                countPulseStep = countPulse;
            }

        }
        state plateau

        when(faultCritical == 0 && arcfault == 0 && faultVacuum == 0 && faultLight == 0 && interlockFlag == 0) {

            interlockCount = 0;
            countPulseStep = countPulse;

            if (isFirstCycle == 1) {} else {
                powerKlystron = powerKlystron + powerStepUp;

                if (powerKlystron >= powerMax) {
                    powerKlystron = powerMax;
                    soakPlateauReached = 1;
                }
                pvPut(powerKlystron, SYNC);

                sprintf(statusMessage, "Cycle:%.3fms,%.1fkW\n", pulseWidth / 1000, powerKlystron);
                pvPut(statusMessage);
		isFirstCycle=1;
            }
        }
        state nominal

        when((faultCritical == 0) && ((faultLight == 1) || (arcfault == 1) || (faultVacuum == 1)) && (interlockFlag == 0)) {
            interlockFlag = 1;
        }
        state PowerControl

        when((faultCritical == 0) && (interlockFlag == 1)) {
            sprintf(statusMessage, "Interlock, Decreasing...\n");
            pvPut(statusMessage);
            if (faultVacuumFlag == 0) {
                faultVacuumFlag = 1;
            }



            if (soakPlateauReached == 1) {
                soakPlateauReached = 0;
            }



	    if (isFirstCycle==1){}else{

		powerKlystron = powerKlystron - powerStepDown;
		countPulseStep = countPulse;
            	if (doubleReducedPower == 1) {
                	powerKlystron = powerKlystron - 2 * powerStepDown;
                	doubleReducedPower = 0;

            	}

            	if (widthFlag) {
                	if ((powerKlystron <= powerMin) && (pulseWidth != pulseWidthMin)) {
                    	pulseWidth = pulseWidth - pulseWidthStep;

                    	powerKlystron = powerMax;
                }

            	}
            	if (pulseWidth <= pulseWidthMin) {
                	pulseWidth = pulseWidthMin;
            	}
            	pvPut(pulseWidth, SYNC);

            	if (powerKlystron<=powerFloor) {
                	powerFloorReached =1;
                	floorSetCount++;
                	powerKlystron = powerFloor;
            	}
		isFirstCycle=1;
	    }
            pvPut(powerKlystron, SYNC);
            sprintf(statusMessage, "Cycle:%.3fms,%.1fkW\n", pulseWidth / 1000, powerKlystron);
            pvPut(statusMessage);
            interlockFlag = 0;

        }
        state nominal

    }

    /*====================================================================*/
    state rearm {

        when(startButton == 0) {

            sprintf(statusMessage, "Operator Stop\n");
            pvPut(statusMessage);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            operationStop = 1;
        }
        state init

     //   when(filamentStatus == 0) {
       //     filamentOffProcedure();
      //  }
       // state init

        when(rearmBlackList == 1 || rearmLimitReached == 1 || fmr==1) {

            sprintf(statusMessage, "Manual Reset Requested...\n");
            pvPut(statusMessage);
            operationStop = 1;
            rearmLimitReached = 0;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            startButton = 0;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        /* Apply 10us pulse length in case of  unsuccessfull rearms */

        when((rearmCount >= 6) && (picted == 1)) {
            if (rearmCount > 9) {
                rearmLimitReached = 1;
            }
            picted = 0;
            sprintf(statusMessage, "Setting 10us...\n");
            pulseWidth = 10.0;
            pvPut(pulseWidth, SYNC);
            mulrearm = 1;
            pvPut(statusMessage);
            operationStop = 0;
            startButton = 1;
	    powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);




        }
        state rearm

	when ((fimState =="RFON")&& (digitizer=="ON")){
	}
	state CycleStart


        /* Rearm LPS and LLRF state machine */

        when((faultCritical == 1) && (rearmBlackList == 0) && (delay(delayTime))) {


            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);
            rearmCount = rearmCount + 1;
            sprintf(statusMessage, "Attempting Reset...\n");
            pvPut(statusMessage);
            sprintf(statusMessage, "Attempting Resetting FIM and SIM...\n");
            pvPut(statusMessage);

	    simReset = 1;
            pvPut(simReset);
            setSimRfOn = 1;
            pvPut(setSimRfOn);

	    sleep(0.2);
	    digitizersp = 2;
            pvPut(digitizersp, SYNC);


	    sleep(0.2);

            digitizersp = 0;
            pvPut(digitizersp, SYNC);

	    sleep(0.2);

 	    fimReset = 1;
	    pvPut(fimReset);
	    sleep(0.2);
	    simReset = 1;
	    pvPut(simReset);
            sleep(0.2);


            fimReset = 1;
            pvPut(fimReset);

            simReset = 1;
            pvPut(simReset);

	    sleep(1);


	    fimState = 3;
            pvPut(fimState, SYNC);

            fimState = 3;
            pvPut(fimState, SYNC);

            sleep(2);

	    digitizersp = 1;
            pvPut(digitizersp, SYNC);




        }
        state rearm

        when(faultCritical == 0) {
            startButton = 1;
            interlockFlag = 0;
            operationStop = 0;
            faultCriticalFlag = 0;
            floorSetCount = 1;
        }
        state CycleStart

    }

    /*================================================================*/
    state plateau {

        when(startButton == 0) {

            sprintf(statusMessage, "Operator Stop\n");
            pvPut(statusMessage);
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            operationStop = 1;
        }
        state init

        when(resetButton == 1) {
            resetEvent = 1;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

 //       when(filamentStatus == 0) {
   //         filamentOffProcedure();
     //   }
      //  state init

        when(rearmBlackList == 1) {

            sprintf(statusMessage, "Manual Reset Requested...\n");
            pvPut(statusMessage);
            operationStop = 1;
            pulseWidth = pulseWidthMin;
            pvPut(pulseWidth, SYNC);
            startButton = 0;
            powerKlystron = powerMin;
            pvPut(powerKlystron, SYNC);

        }
        state init

        when(faultCritical == 1 && rearmBlackList == 0) {
            faultCriticalFlag = 1;
            tempPower = pvGet(powerKlystron);
            tempWidth = pvGet(pulseWidth);
            delayTime = fimRecoveryTime;
            if (faultVacuum == 1) {
                delayTime = vacuumRecoveryTime;
            }

        }
        state rearm

        when((faultCritical == 0) && ((faultLight == 1) || (arcfault == 1) ||(faultVacuum == 1))) {
            interlockFlag = 1;
           // tempPower = pvGet(powerKlystron);
           // tempWidth = pvGet(pulseWidth);

            if (faultCriticalVacuum == 1) {
                faultVacuumCriticalFlag = 1;
            }

        }
        state PowerControl

        when(faultCritical == 1) {
            sprintf(statusMessage, "Interlock during the plateau\n");
            pvPut(statusMessage);
        }
        state rearm

        when(delay(pulsePlateauTime)) {

            sprintf(statusMessage, "Cycle Finished!!\n");

            pvPut(statusMessage);
            if (pulseWidth < pulseWidthMin) {
                pulseWidth = pulseWidth + 100.00;
            } else {
                pulseWidth = pulseWidth + pulseWidthStep;
            }
            if (pulseWidth > 10) {
                tempwid = pulseWidth;
            }
            if (enuma == 1) {
                mulrearm = 0;
                enuma = 0;
                pulseWidth = tempwid;

            }

            if (pulseWidth >= pulseWidthMax) {
                pulseWidth = pulseWidthMax;
                cycleCount++;

                if (maxWidthConditioning == 1) {
                    cycleCount = 0;
                    pulseWidth = pulseWidthMax;
                }

                if (cycleCount == 2 && neverStop == 1 && maxWidthConditioning == 0) {
                    cycleCount = 0;
                    pulseWidth = pulseWidthMin;
                }
            }

        }
        state CycleStart
    }

}
