/* FIM State machine enumeration */

enum FIM {
  IDLE = 0,
  ARM = 1,
  HVON = 2,
  RFON = 3,
  ABO = 4
};
