# 1.4.0
* Include FIM logging interlocks to be used on RFQ IOC
* Include correct support for RFQ

# 1.3.0
* Included arc detector handling
* Bump EPICS base to 7.0.8.1
