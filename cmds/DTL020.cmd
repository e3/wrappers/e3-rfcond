require essioc
require rfcond

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")
epicsEnvSet("EPICS_PVA_ADDR_LIST", "172.16.63.255")
epicsEnvSet("EPICS_CA_ADDR_LIST", "172.16.63.255")
# General
epicsEnvSet("M", "DTL-020:conda")
epicsEnvSet("EVR", "DTL-020:RFS-EVR-101:")
epicsEnvSet("LLRF", "DTL-020:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "DTL-020:RFS-DIG-101:")
epicsEnvSet("FIM", "DTL-020:RFS-FIM-101:")
epicsEnvSet("RFS", "DTL-020:RFS-")
epicsEnvSet("VAC1","DTL-020:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2","DTL-020:Vac-VGC-50000:PrsR CPP")
epicsEnvSet("VAC3","")
epicsEnvSet("VAC4","")
epicsEnvSet("VACI1","DTL-020:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","DTL-020:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","DTL-020:RFS-FIM-101:DI10-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","DTL-020:RFS-FIM-101:DI12-Ilck-RB.RVAL CPP")

#Load standard module startup scripts
#iocshLoad("$(essioc_DIR)/common_config.iocsh")


iocshLoad("$(rfcond_DIR)/rfcond.iocsh")
#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
# Call iocInit to start the IOC
#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
iocInit()
date
