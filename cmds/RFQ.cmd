require essioc
require rfcond

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")
epicsEnvSet("EPICS_PVA_ADDR_LIST", "172.16.63.255")
epicsEnvSet("EPICS_CA_ADDR_LIST", "172.16.63.255")
# General
epicsEnvSet("M", "RFQ-010:conda")
epicsEnvSet("EVR", "RFQ-010:RFS-EVR-101:")
epicsEnvSet("EVR", "RFQ-010:RFS-EVR-101:")
epicsEnvSet("LLRF", "RFQ-010:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "RFQ-010:RFS-DIG-101:")
epicsEnvSet("FIM", "RFQ-010:RFS-FIM-101:")
epicsEnvSet("RFS", "RFQ-010:RFS-")
epicsEnvSet("VAC1", "RFQ-010:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2", "RFQ-010:Vac-VGC-40000:PrsR CPP")
epicsEnvSet("VAC3", "RFQ-010:Vac-VGC-20000:PrsR CPP")
epicsEnvSet("VAC4", "RFQ-010:Vac-VGC-30000:PrsR CPP")
epicsEnvSet("VACI1","RFQ-010:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","RFQ-010:RFS-VacMon-120:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","RFQ-010:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","RFQ-010:RFS-VacMon-140:Status-Ilck-RB.RVAL CPP")


#Load standard module startup scripts
#iocshLoad("$(essioc_DIR)/common_config.iocsh")


#- Load FIM interlock list
iocshLoad("$(rfcond_DIR)/fim-interlock-list.iocsh", "M=${M}:")



iocshLoad("$(rfcond_DIR)/rfcond.iocsh")
#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
# Call iocInit to start the IOC
#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
iocInit()
date
