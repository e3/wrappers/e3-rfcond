require essioc
require rfcond

epicsEnvSet("ENGINEER", "Gabriel Fedel <gabriel.fedel@ess.eu>")
## Add extra environment variables here
epicsEnvSet("TOP",      "$(E3_CMD_TOP)")
epicsEnvSet("EPICS_PVA_ADDR_LIST", "172.16.63.255")
epicsEnvSet("EPICS_CA_ADDR_LIST", "172.16.63.255")
# General
epicsEnvSet("M", "DTL-010:conda")
epicsEnvSet("EVR", "DTL-010:RFS-EVR-101:")
epicsEnvSet("LLRF", "DTL-010:RFS-LLRF-101:")
epicsEnvSet("LLRFD", "DTL-010:RFS-DIG-101:")
epicsEnvSet("FIM", "DTL-010:RFS-FIM-101:")
epicsEnvSet("RFS", "DTL-010:RFS-")
epicsEnvSet("VAC1","DTL-010:Vac-VGC-10000:PrsR CPP")
epicsEnvSet("VAC2","DTL-010:Vac-VGC-50000:PrsR CPP")
epicsEnvSet("VAC3","")
epicsEnvSet("VAC4","")
epicsEnvSet("VACI1","DTL-010:RFS-VacMon-110:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI2","DTL-010:RFS-VacMon-120:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI3","DTL-010:RFS-VacMon-130:Status-Ilck-RB.RVAL CPP")
epicsEnvSet("VACI4","DTL-010:RFS-VacMon-140:Status-Ilck-RB.RVAL CPP")



#Load standard module startup scripts
#iocshLoad("$(essioc_DIR)/common_config.iocsh")

iocshLoad("$(rfcond_DIR)/rfcond.iocsh")

#- Load FIM interlock list
iocshLoad("$(rfcond_DIR)/fim-interlock-list.iocsh")

#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
# Call iocInit to start the IOC
#afterInit("caPutJsonLogInit 127.0.0.1:8002 -1")
iocInit()
date
